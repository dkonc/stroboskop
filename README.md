# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

git clone https://dkonc@bitbucket.org/dkonc/stroboskop.git

Naloga 6.2.3:

https://bitbucket.org/dkonc/stroboskop/commits/b053c0fc2676837c4347b58decf59718edfc6ddc?at=master

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/dkonc/stroboskop/commits/f1d4f8593421158c8fd794cfba5e1dae7b660181?at=master

Naloga 6.3.2:
https://bitbucket.org/dkonc/stroboskop/commits/00b3631d933c9efbad949c3c769349b6f0e4a0c1?at=master

Naloga 6.3.3:
https://bitbucket.org/dkonc/stroboskop/commits/33a2aa9534218984bf40b1d898f8fd2e277ad1b7?at=master

Naloga 6.3.4:
https://bitbucket.org/dkonc/stroboskop/commits/8f9b421877bb42fb1d4995a59dfcc4592de5b582?at=master

Naloga 6.3.5:

git checkout master
git merge izgled

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/dkonc/stroboskop/commits/78d74721bba6a824a7356850c2cf7f5388713658?at=master

Naloga 6.4.2:
https://bitbucket.org/dkonc/stroboskop/commits/1131c63f05f8799b9bb14273673663a761130aae?at=master

Naloga 6.4.3:
https://bitbucket.org/dkonc/stroboskop/commits/fd28803ed29d79327ae832d9708268373d8c7468?at=master

Naloga 6.4.4:
https://bitbucket.org/dkonc/stroboskop/commits/b575bcfcb4432802f7ee89e33d7f624ed5f9f4f5?at=master